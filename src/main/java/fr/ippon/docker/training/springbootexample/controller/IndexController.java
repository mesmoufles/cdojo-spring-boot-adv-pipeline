package fr.ippon.docker.training.springbootexample.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.ippon.docker.training.springbootexample.dto.InfoDTO;

/**
 * HelloControler
 */
@RestController
@RequestMapping("/api")
public class IndexController {

    @GetMapping("hello")
    public InfoDTO index() throws UnknownHostException {
        InfoDTO info = new InfoDTO();
        info.setCurrentDateTime(LocalDateTime.now());
        info.setHostName(InetAddress.getLocalHost().getHostName());
        return info;
    }
}